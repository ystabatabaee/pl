#lang racket
; EOPL-4.9
; Implementation of store with vector
; What is missed in this representation? We have constant-time access
; and update in vector because vector-set! and vector-ref are both
; executed in constant time. But vector-append creates a fresh vector
; that contains all the elements of the previous vector and apppends
; the new element to it.  vector in order  

(define empty-store
  (lambda ()
    #()))

(define the-store 'uninitialized)

(define get-store
  (lambda ()
    the-store))

(define initialize-store!
  (lambda ()
    (set! the-store (empty-store))))

(define reference?
  (lambda (v)
    (integer? v)))

(define newref
  (lambda (val)
    (let ((next-ref (vector-length the-store)))
      (set! the-store (vector-append the-store (vector val)))
      next-ref)))

(define deref
  (lambda (ref)
    (vector-ref the-store ref)))

(define setref!
  (lambda (ref val)
    (vector-set! the-store ref val)))










