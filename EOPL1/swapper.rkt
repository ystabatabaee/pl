#lang racket
; EOPL-1.18

(define swapper
  (lambda (s1 s2 slist)
    (if (null? slist)
        '()
        (cond
          ((eqv? s1 (car slist))
            (cons s2 (swapper s1 s2 (cdr slist))))
          ((eqv? s2 (car slist))
            (cons s1 (swapper s1 s2 (cdr slist))))
          (else
           (if (list? (car slist))
               (cons (swapper s1 s2 (car slist)) (swapper s1 s2 (cdr slist)))
               (cons (car slist) (swapper s1 s2 (cdr slist)))))))))

            


